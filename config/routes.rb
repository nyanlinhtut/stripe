Rails.application.routes.draw do
  get 'charges/index'
  
  resources :charges

  root 'charges#index'
end
