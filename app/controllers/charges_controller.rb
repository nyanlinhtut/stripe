class ChargesController < ApplicationController

	def index
	end

	rescue_from Stripe::CardError, with: :catch_exception
  def new
  end

  def create
  # Amount in cents
  @amount = 500

  customer = Stripe::Customer.create(
    :email => params[:stripeEmail],
    :source  => params[:stripeToken]
  )

  charge = Stripe::Charge.create(
    :customer    => customer.id,
    :amount      => @amount,
    :description => 'Rails Stripe customer',
    :currency    => 'usd'
  )
  end	
  
  def catch_exception(exception)
    flash[:error] = exception.message
  end
end
